#!/usr/bin/lua5.1
-- simple Offsetfinder-first-version
require("imlib2")
local im = imlib2.image.load("test.png")

function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

local xlength = im:get_width()
local xmid = round(xlength/2,0)
local xheight = im:get_height()
local xhalf = round(xheight/2,0)

  for y=0, xhalf do
     c1 = im:get_pixel(xmid, y)
          c1a = (c1.alpha)
          if c1a < 200 then temp_top=y  io.stdout:write( "top_margin=",y+1, "\n")  end
  end

  for y=xhalf, xheight-1 do
     c1 = im:get_pixel(xmid, y)
          c1a = (c1.alpha)
          if c1a < 200 then  io.stdout:write("bottom_margin=",xheight-y, "\n")  end
  end

  for x=0, xmid do
     c1 = im:get_pixel(x, xhalf)
          c1a = (c1.alpha)
          if c1a < 200 then temp_top=y  io.stdout:write( "left_margin=",x+1, "\n")  end
  end

  for x=xmid, xlength-1 do
     c1 = im:get_pixel(x, xhalf)
          c1a = (c1.alpha)
          if c1a < 200 then  io.stdout:write("right_margin=",xlength-x, "\n")  end
  end

io.stdout:write( "top_margin=0", "\n")
io.stdout:write( "bottom_margin=0", "\n")
io.stdout:write( "left_margin=0", "\n")
io.stdout:write( "right_margin=0", "\n")
io.stdout:write( "orig_height=",xheight, "\n")
io.stdout:write( "orig_length=",xlength, "\n")

