#!/usr/bin/lua5.1
-- simple gradientfinder-first-version
require("imlib2")
local im =  imlib2.image.load("gradient.png")

printf = function(s,...)
           return io.write(s:format(...))
         end

function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

function twodigits(a, b) print(round(a,b), round2(a,b)) end

io.stdout:write("background-image: linear-gradient(to bottom, \n")
local xlength = im:get_width()
local xmid = xlength/2
local xheight = im:get_height()

  for y=0, xheight-2 do
     c1 = im:get_pixel(xmid, y)
          c1r = (c1.red)
          c1g = (c1.green)
          c1b = (c1.blue)
          percentorig = y/(xheight-1) *100
          percent = round(percentorig, 2)
          io.stdout:write("rgb(",c1r,",",c1g,",",c1b,") ", percent, "%,\n") 
  end

   for y=xheight-1 , xheight-1 do
     c1 = im:get_pixel(xmid, y)
          c1r = (c1.red)
          c1g = (c1.green)
          c1b = (c1.blue)
          percentorig = y/(xheight-1) *100
          percent = round(percentorig, 2)
          io.stdout:write("rgb(",c1r,",",c1g,",",c1b,") ", percent, "%);\n") 
  end


