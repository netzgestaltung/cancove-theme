#!/usr/bin/lua5.1
-- simple gradientfinder-first-version
require("imlib2")
local im =  imlib2.image.load("cropped.png")

function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

io.stdout:write("border-style:solid;\nborder-width: 1px;\n")

local xlength = im:get_width()
local xmid = round(xlength/2,0)
local xheight = im:get_height()
local xhalf = round(xheight/2,0)

          c1 = im:get_pixel(xmid, 0)
          c1r = (c1.red)
          c1g = (c1.green)
          c1b = (c1.blue)
          io.stdout:write("border-top: rgb(",c1r,",",c1g,",",c1b,") ;", "\n") 

          c2 = im:get_pixel(xmid, xheight-1)
          c2r = (c2.red)
          c2g = (c2.green)
          c2b = (c2.blue)
          io.stdout:write("border-bottom: rgb(",c2r,",",c2g,",",c2b,") ;", "\n") 

          c3 = im:get_pixel(0,xhalf )
          c3r = (c3.red)
          c3g = (c3.green)
          c3b = (c3.blue)
          io.stdout:write("border-right: rgb(",c3r,",",c3g,",",c3b,") ;", "\n") 

          c3 = im:get_pixel(xlength-1,xhalf )
          c3r = (c3.red)
          c3g = (c3.green)
          c3b = (c3.blue)
          io.stdout:write("border-left: rgb(",c3r,",",c3g,",",c3b,") ;", "\n") 

