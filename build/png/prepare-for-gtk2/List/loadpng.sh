
cp $1.png test.png;
rm test.scss

_import_from_lua() {
lua offset-finder.lua > offset-analyz;
echo "#!/usr/bin/env bash"
cat offset-analyz | sort -r | grep  -m 1 top;
cat offset-analyz | sort -r | grep  -m 1 bottom;
cat offset-analyz | sort -r | grep  -m 1 left;
cat offset-analyz | sort -r | grep  -m 1 right;
cat offset-analyz | grep  -m 1 height;
cat offset-analyz | grep  -m 1 length;
rm  offset-analyz;
}

_cleanup() {
rm offset-list
rm cropped.png
rm gradient.png
rm test.scss
rm test.png
}

_import_from_lua > offset-list;
source offset-list;

convert test.png -crop $(($orig_length - $left_margin - $right_margin))x$(($orig_height - $bottom_margin - $top_margin))+$left_margin+$top_margin cropped.png
convert test.png -crop $(($orig_length - $left_margin - $right_margin-2))x$(($orig_height - $bottom_margin - $top_margin-2))+$(($left_margin+1))+$(($top_margin +1)) gradient.png

echo "$1 { " >> test.scss
lua border-find.lua >> test.scss
lua gradientfinder.lua >> test.scss
echo "}" >> test.scss

cp test.scss $1.scss

_cleanup 
